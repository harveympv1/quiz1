﻿using quizdosHarvey.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace quizdosHarvey
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }
        public void ButtonClick(object sender, EventArgs e)
        {
            // crear objeto del modelo tarea
            Comidas comidas = new Comidas()
            {
                nombre_comida = Name.Text
            };
            using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlBd))
            {
                // crear tabla en base de datos
                connection.CreateTable<Comidas>();

                // crear registro en la tabla
                var result = connection.Insert(comidas);

                if (result > 0)
                {
                    DisplayAlert("Correcto", "La tarea se creo correctamente", "OK");
                }
                else
                {
                    DisplayAlert("Incorrecto", "La tarea no fue creada", "OK");
                }
            }

            // llame al otro metodo
            listar();
        }



        public void listar()
        {
            using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlBd))
            {

                List<Comidas> listaTareas;
                listaTareas = connection.Table<Comidas>().ToList();

                mirarcomida.ItemsSource = listaTareas;

            }
        }
    }
}

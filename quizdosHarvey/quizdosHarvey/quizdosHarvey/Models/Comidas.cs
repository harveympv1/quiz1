﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace quizdosHarvey.Models
{
    class Comidas
    {
            [PrimaryKey, AutoIncrement, Column("_id")]
            public int Id { get; set; }

            [MaxLength(150)]
            public string nombre_comida { get; set; }
        
    }
}
